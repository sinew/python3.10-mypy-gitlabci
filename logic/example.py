import enum


class Bit(enum.Enum):
    zero = 0
    one = 1


class Gate:

    @staticmethod
    def _or(a: Bit, b: Bit) -> Bit:
        match (a, b):
            case (a.one, _) | (_, b.one):
                return Bit.one
            case _:
                return Bit.zero

    @staticmethod
    def _and(a: Bit, b: Bit) -> Bit:
        match (a, b):
            case (a.one, b.one):
                return Bit.one
            case _:
                return Bit.zero

    @staticmethod
    def _xor(a: Bit, b: Bit) -> Bit:
        match (a, b):
            case (Bit.one, Bit.zero) | (Bit.zero, Bit.one):
                return Bit.one
            case _:
                return Bit.zero