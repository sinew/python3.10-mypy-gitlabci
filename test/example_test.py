import unittest

from logic.example import Gate, Bit


class ExampleTest(unittest.TestCase):

    def test__or(self) -> None:
        self.assertEqual(Gate._or(Bit.one, Bit.zero), Bit.one, "1 OR 0 == 1")
        self.assertEqual(Gate._or(Bit.zero, Bit.one), Bit.one, "0 OR 1 == 1")
        self.assertEqual(Gate._or(Bit.one, Bit.one), Bit.one, "1 OR 1 == 1")
        self.assertEqual(Gate._or(Bit.zero, Bit.zero), Bit.zero, "0 OR 0 == 0")

    def test__and(self) -> None:
        self.assertEqual(Gate._and(Bit.one, Bit.one), Bit.one, "1 AND 1 == 1")
        self.assertEqual(Gate._and(Bit.one, Bit.zero), Bit.zero, "1 AND 0 == 0")
        self.assertEqual(Gate._and(Bit.zero, Bit.one), Bit.zero, "0 AND 1 == 0")
        self.assertEqual(Gate._and(Bit.zero, Bit.zero), Bit.zero, "0 AND 0 == 0")

    def test__xor(self) -> None:
        self.assertEqual(Gate._xor(Bit.one, Bit.zero), Bit.one, "1 XOR 0 == 1")
        self.assertEqual(Gate._xor(Bit.zero, Bit.one), Bit.one, "0 XOR 1 == 1")
        self.assertEqual(Gate._xor(Bit.one, Bit.one), Bit.zero, "1 XOR 1 == 0")
        self.assertEqual(Gate._xor(Bit.zero, Bit.zero), Bit.zero, "0 XOR 0 == 0")
